#pragma once

#include <ctime>
#include <string>
#include <vector>
#include "Storage.h"

using namespace std;

class Calender
{
public:
	Calender();
	~Calender();
	void LoadEvents();
	void CreateEvent(tm time, string name, string day);
	void ShowEvents();
	void ShowEvent(int n);
	tm CreateTime(int year, int month, int date, string dayOfTheWeek);
	int CountOfEvents();
	int FindMatchingEvents(tm time);
	int FindMatchingEvents(string name);



	tm dateOfevent;
	string nameOfEvent;
	string dayOftheWeek;
	vector<Storage> s1;

private:
	int countOfEvents = 0;
};


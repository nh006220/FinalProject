#pragma once

using namespace std;

#include <string>

struct Storage
{
	string NameOfEvent;
	string DayOfEvent;
	tm DateOfEvent;
	int EventNumber;

	Storage(string n, string d, tm t, int num);
};

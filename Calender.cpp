#include "pch.h"
#include "Calender.h"
#include "Caleander Bot.h"
#include <string>
#include <iostream>
#include <fstream> 
#include <sstream> 
#include <algorithm>

using namespace std;

Calender::Calender()
{

}

Calender::~Calender()
{
}

//Load events from the text file into the storage class
void Calender::LoadEvents()
{
	string s;
	try
	{
		//Read the text file into a string
		ifstream t(file);
		stringstream buffer;
		buffer << t.rdbuf();
		s = buffer.str();
	}
	catch (const std::exception&)
	{
		//return an error is the text file cannot be read
		cout << "Unable to load events" << endl;
	}

	//split the string per line
	vector<string> vectorReturn = Split(s, '\n');

	for (size_t i = 0; i < vectorReturn.size()-1; i++)
	{
		//split the string per commer
		vector<string> vectorTemp = Split(vectorReturn[i], ',');

		//store the elements of the vector into variables
		string timeString = vectorTemp[0];
		string Name = vectorTemp[1];
		int eventNumber = stoi(vectorTemp[2]);
		string dayOfWeek = vectorTemp[3];

		//Set the day of the week to blank if its not found
		if (dayOfWeek.empty())
		{
			dayOfWeek = "";
		}

		//Split the date time of the string
		vector<string> timeVector = Split(timeString, '-');

		int year, month, day;

		//set the values for the date time of the string
		year = stoi(timeVector[0]);
		month = stoi(timeVector[1]);
		day = stoi(timeVector[2]);

		int lastEvent = CountOfEvents();

		//only add the new event to the storage class if it does not exist
		if (eventNumber >= lastEvent)
		{
			tm time = CreateTime(year, month, day, "");

			Storage temp(Name, dayOfWeek, time, eventNumber);

			s1.push_back(temp);
		}
	}
}

//Create a new event
void Calender::CreateEvent(tm time, string name, string day)
{
	int lastEvent = CountOfEvents();

	fstream fs;

	//in opens the file for reading, out opens the file for writing, app means append
	fs.open(file, fstream::in | fstream::out | fstream::app);

	//Writes the values passed into the text file
	fs << time.tm_year << "-" << time.tm_mon << "-" << time.tm_mday << "," << name << "," << lastEvent << "," << day <<  endl;

	fs.close();

	//Call load events to load the new event into the storage calss
	LoadEvents();

	cout << name << " Has been created" << endl;
}

//Show all the events in the stoage class
void Calender::ShowEvents()
{
	for (size_t i = 0; i < s1.size(); i++)
	{
		string timeString = DisplayDateTime(s1[i].DateOfEvent);

		cout << s1[i].NameOfEvent << "is on " << timeString << endl;
	}
}

//Show a specific event
void Calender::ShowEvent(int n)
{
	try
	{
		//thow an error if an invalid value is passed
		if (s1[n].EventNumber >= 0 && n < s1.size()+1)
		{
			string timeString = DisplayDateTime(s1[n].DateOfEvent);

			cout << s1[n].NameOfEvent << "is on " << timeString << endl;
		}
		else
		{
			throw;
		}
	}
	catch (const std::exception&)
	{
		cout << "I can't find the event" << endl;
	}
}

//Create a tm struct with the year, month and date values
tm Calender::CreateTime(int year, int month, int date, string day)
{
	dayOftheWeek = day;

	time_t t1 = time(0);
	tm *timeinfo = localtime(&t1);

	time(&t1);
	timeinfo = localtime(&t1);
	timeinfo->tm_year = year;
	timeinfo->tm_mon = month;
	timeinfo->tm_mday = date;

	return *timeinfo;
}

//return the number of events stored
int Calender::CountOfEvents()
{
	int last = s1.size();

	if (last <= 0)
	{
		return 0;
	}
	else
	{
		return s1.size();
	}
}

//Find mathing events based on the time of the event
int Calender::FindMatchingEvents(tm time)
{
	int found = 0;

	for (size_t i = 0; i <= s1.size(); i++)
	{
		if (s1[i].DateOfEvent.tm_year == time.tm_year)
		{
			if (s1[i].DateOfEvent.tm_mon == time.tm_mon)
			{
				if (s1[i].DateOfEvent.tm_mday == time.tm_mday)
				{
					if (i == 0)
					{
						i = 1;
					}

					found = i;

					break;
				}
			}
		}
	}

	return found;
}

//Find matching events based on the name of the event
int Calender::FindMatchingEvents(string name)
{
	int found = 0;

	string eventToFind;

	//Trim any whitespace at the start or end of the string
	name = TrimString(name);

	if (ValidateInput(name))
	{
		for (size_t i = 0; i < s1.size(); i++)
		{
			//Trim any whitespace at the start or end of the string
			eventToFind = TrimString(s1[i].NameOfEvent);

			if (name == eventToFind)
			{
				/*if (i == 0)
				{
					i++;
				}*/
				found = i;

				break;
			}
		}
	}
	else
	{
		cout << "Can't find event with this name" << endl;
	}

	return found;
}
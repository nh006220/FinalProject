#include "pch.h"
#include <iostream>
#include "Calender.h"
#include <ctime>
#include <regex>
#include <iomanip>
#include <sstream>
#include <vector>
#include <fstream>
#include "Caleander Bot.h"
#include <locale>

using namespace std;

const string daysOfTheWeek[] = {"monday", "tuesday", "wednedsay", "thursday", "friday", "saturday", "sunday"};
const string MonthsOfTheYear[] = {"january", "febuary", "march", "april", "may", "june", "juy", "august", "september", "october", "noverber", "december"};
const string WordsToCreate[] = {"is on", "on", "at"};
const string WordsForShow[] = {"when is", "when's", "whats on"};
const string KeyWords[] = {"create", "show", "delete"};

string file = "Storage.txt";

int main()
{
	Calender c1;

	//Load all current events from text file
	c1.LoadEvents();

	cout << "Welcome to your calendar" << endl;
	cout << "You can say ask me to create and event or show one of the events you have created" << endl << endl;
	cout << "Here are your current events" << endl;

	c1.ShowEvents();

	while (true)
	{
		int date, month, year;
		string day, userInput, eventName, type;

		cout << ">> ";

		getline(cin, userInput);

		if (userInput == "")
			continue;

		if (ValidateInput(userInput) == false)
		{
			cout << "Input is not valid" << endl;
		}

		vector<string> testThing;

		CreateRegx(testThing);

		vector<string> testThing1 = FindInString(testThing, userInput);

		type = DetermineUserInput(userInput);

		if (type == "Create")
		{
			if (GetDateFromString(eventName, userInput, day, date, month, year))
			{
				GetEventNameFromString(userInput, eventName);

				tm myTime = c1.CreateTime(year, month, date, day);

				c1.CreateEvent(myTime, eventName, day);
			}
		}
		else if (type == "Show")
		{
			//Look if the user has enterd the date of the event
			if (GetDateFromString(eventName, userInput, day, date, month, year))
			{
				tm myTime = c1.CreateTime(year, month, date, day);

				int eventID = c1.FindMatchingEvents(myTime);

				if (eventID != 0)
				{
					c1.ShowEvent(eventID);
				}
				else
				{
					cout << "I cant find any events" << endl;
				}
			}
			//Look if the user has enterd the name of the event
			else
			{
				GetEventNameFromString(userInput, eventName);

				if (eventName.empty() == false)
				{
					int eventToShow = c1.FindMatchingEvents(eventName);

					try
					{
						c1.ShowEvent(eventToShow);
					}
					catch (const std::exception &)
					{
						cout << "I could not find the event" << endl;
					}
				}
			}
		}
		else if (type == "Remove")
		{
			if (GetDateFromString(eventName, userInput, day, date, month, year))
			{
				tm myTime = c1.CreateTime(year, month, date, day);
			}
			else
			{
				//GetEventNameFromString(userInput, eventName);

				eventName;
			}
		}
	}
}

void CreateRegx(std::vector<std::string> &testThing)
{
	for (size_t i = 0; i < WordsForShow->size() - 1; i++)
	{
		if (WordsForShow[i].length() > 1)
		{
			testThing.push_back("(" + WordsForShow[i] + ")");
		}
	}
	for (size_t i = 0; i < WordsToCreate->size() - 1; i++)
	{
		if (WordsToCreate[i].length() > 1)
		{
			testThing.push_back("(" + WordsToCreate[i] + ")");
		}
	}
	for (size_t i = 0; i < KeyWords->size() - 1; i++)
	{
		if (KeyWords[i].length() > 1)
		{
			testThing.push_back("(" + KeyWords[i] + ")");
		}
	}
}

//Determine the action the user wants to take based on input
string DetermineUserInput(string &userInput)
{
	string type;

	int foundCount = 0;

	smatch match;
	for (size_t i = 0; i < WordsForShow->size() - 1; i++)
	{
		//Get text beofre WordsToUse
		regex rgx("(" + WordsForShow[i] + ")");

		//Will return true if the a match is found
		if (regex_search(userInput, match, rgx))
		{
			string matchStr1 = match[0].str();

			if (matchStr1 == WordsForShow[i])
			{
				type = "Show";

				foundCount++;

				break;
			}
		}
	}
	for (size_t i = 0; i <= WordsToCreate->size() - 1; i++)
	{
		//Get text beofre WordsToUse
		regex rgx("(" + WordsToCreate[i] + ")");

		//Will return true if the a match is found
		if (regex_search(userInput, match, rgx))
		{
			string matchStr3 = match[0].str();

			if (matchStr3 == WordsToCreate[i])
			{
				type = "Create";

				foundCount++;

				break;
			}
		}
	}
	if (type.empty() == false)
	{
		if (foundCount > 1)
		{
			cout << "Enter the number of the action you want to take" << endl;
			cout << "1. Create a new event" << endl;
			cout << "2. Show an event" << endl;
			cout << ">> ";

			int selected;

			cin >> selected;

			if (selected == 1)
			{
				type = "Create";
			}
			else if (selected == 2)
			{
				type = "Show";
			}
			else
			{
				type = "Not Found";
			}
		}
	}

	return type;
}

//Look for the event name in the string
void GetEventNameFromString(string &userInput, string &eventName)
{
	//Store event name
	smatch matchEventName;
	bool eventNameFound = false;

	int wordsToCreateSize = WordsToCreate->size();

	for (size_t i = 0; i < 3; i++)
	{
		//Get text beofre WordsToUse
		regex rgx("(" + WordsToCreate[i] + ")");

		//Will return true if the a match is found
		if (regex_search(userInput, matchEventName, rgx))
		{
			//Store string before words to use as eventName
			eventName = matchEventName.prefix().str();

			eventNameFound = true;

			break;
		}
	}
	for (size_t i = 0; i < 3; i++)
	{
		//Get text beofre WordsToUse
		regex rgx("(" + WordsForShow[i] + ")");

		//Will return true if the a match is found
		if (regex_search(userInput, matchEventName, rgx))
		{
			//Store string before words to use as eventName

			if (eventNameFound == false && eventName != "")
			{
				eventName = "AskUser";

				break;
			}
			else
			{
				eventName = matchEventName.suffix().str();

				break;
			}
		}
	}
	if (eventName.empty() || eventName == "AskUser")
	{
		cout << "What is the name of your event? " << endl;

		string inputEventName;

		cin.ignore();
		getline(cin, inputEventName);

		if (ValidateInput(inputEventName) == 1)
		{
			cout << "Event name is not valid" << endl;

			eventName = "Empty event name";
		}
		else
		{
			eventName = inputEventName;
		}
	}
}

//Return false if string is valid, if not return true
bool ValidateInput(string &userInput)
{
	bool stringCheck = false;

	//Check the user enterd something
	if (userInput.length() < 1)
	{
		stringCheck = true;
	}

	for (size_t i = 0; i < userInput.length(); i++ && stringCheck == true)
	{
		//Check to make sure the character is alphanumeric or whitespace
		if (ispunct(userInput[i]) == 1)
		{
			stringCheck = true;
		}

		//Convert the character to lowecase
		userInput[i] = tolower(userInput[i]);

		stringCheck = true;
	}

	return stringCheck;
}

//look for the date time in the string
bool GetDateFromString(string &eventName, string &s, string &day, int &date, int &month, int &year)
{
	bool foundDay = false, foundDate = false, foundMonth = false, foundYear = false;

	//Create var to store resaults
	smatch matchDaysOfTheWeek, matchDateNumber, matchMonthsOfTheYear, matchYear;
	string suffixMatchDaysOfTheWeek, suffixMatchYear;

	//Look for day of the week
	for (size_t i = 0; i < daysOfTheWeek->size(); i++)
	{
		regex rgx("(" + daysOfTheWeek[i] + ")[^ ]?");

		//Will return true if the a match is found
		if (regex_search(s, matchDaysOfTheWeek, rgx))
		{
			day = matchDaysOfTheWeek.str();

			//Only sent event name if its not already set
			if (eventName == "No Event" && eventName.empty() == true)
			{
				eventName = matchDaysOfTheWeek.prefix().str();
			}

			break;
		}
	}
	if (day.empty())
	{
		//If no day is found set it to an empty string
		day = "";
	}
	else
	{
		foundDay = true;
	}

	//Look for the date
	if (matchDaysOfTheWeek.suffix().str() != "" || day == "")
	{
		string search;

		if (matchDaysOfTheWeek.suffix().str() == "")
		{
			search = s;
		}
		else
		{
			search = matchDaysOfTheWeek.suffix().str();
		}

		regex rgx("[0-9]{1,2}");

		if (regex_search(search, matchDateNumber, rgx))
		{
			try
			{
				//Store date if it is valid
				date = stoi(matchDateNumber.str());

				foundDay = true;

				//Write the suffix into a new variable. this is because if you call suffix more than once it will get a memory read error.
				suffixMatchDaysOfTheWeek = matchDateNumber.suffix().str();
			}
			catch (const exception &)
			{
				//if not date is found set it to -1
				date = -1;
			}
		}
	}

	//Look for month
	if (suffixMatchDaysOfTheWeek != "")
	{
		for (size_t i = 0; i < MonthsOfTheYear->length(); i++)
		{
			regex rgx("(" + MonthsOfTheYear[i] + ")[^ ]?");

			//Will return true if the a match is found
			if (regex_search(suffixMatchDaysOfTheWeek, matchMonthsOfTheYear, rgx))
			{
				month = i + 1;

				foundMonth = true;

				suffixMatchYear = matchMonthsOfTheYear.suffix().str();

				break;
			}
		}
		if (month == NULL)
		{
			month = -1;
		}
	}

	//Look for the year
	if (suffixMatchYear != "")
	{
		regex rgxYear("\\d\\d\\d\\d");

		if (regex_search(suffixMatchYear, matchYear, rgxYear))
		{
			try
			{
				year = stoi(matchYear.str());

				foundYear = true;
			}
			catch (const exception &)
			{
				year = -1;
			}
		}
		else
		{
			//if there is no year use the current year
			SetYearToCurrentYear(year);

			foundYear = true;
		}
	}
	else
	{
		SetYearToCurrentYear(year);

		foundYear = true;
	}

	if (foundDay == true && foundMonth == true && foundYear == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//get the current year
void SetYearToCurrentYear(int &year)
{
	time_t thisYear = time(0);
	tm *now = localtime(&thisYear);
	year = 1900 + now->tm_year;
}

//split string on delimiting chaaracter into vector
vector<string> Split(string &s, char del)
{
	vector<string> vectorReturn;

	string token;

	stringstream ss(s);
	while (ss.good())
	{
		string temp;
		getline(ss, temp, del);
		vectorReturn.push_back(temp);
	}

	return vectorReturn;
}

//find value in string
vector<string> FindInString(vector<string> rgx, string userInput)
{
	vector<string> returnVector;

	smatch match;
	for (size_t i = 0; i < rgx.size(); i++)
	{
		//Get text beofre WordsToUse
		regex rgx(rgx[i]);

		//Will return true if the a match is found
		if (regex_search(userInput, match, rgx))
		{
			returnVector.push_back(match.str());

			break;
		}
	}

	return returnVector;
}

//Trim whitespace at start and end of string
string TrimString(string str1)
{
	int space = str1.find_first_of(" ");
	int nonSpace = str1.find_first_not_of(" ");

	try
	{
		int startOfString = 0;

		if (space == 0)
		{
			//remove white space at start of string
			str1.replace(space, nonSpace, "");
		}
	}
	catch (const std::exception &)
	{
	}

	int space2 = str1.find_last_of(" ");
	int nonSpace2 = str1.find_last_not_of(" ");

	try
	{
		int endOfString = str1.size() - 1;

		if (space2 == endOfString)
		{
			//Remove white space at end of sting
			str1.replace(nonSpace2 + 1, endOfString + 1, "");
		}
	}
	catch (const std::exception &)
	{
	}

	return str1;
}

//Convert date time to string
string DisplayDateTime(tm time)
{
	char timeChar[100];

	tm tempTime = time;
	tempTime.tm_year = tempTime.tm_year - 1900;

	strftime(timeChar, 50, "%B %d, %Y", &tempTime);

	string dateString = timeChar;

	return timeChar;
}
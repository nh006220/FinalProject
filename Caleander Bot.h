#pragma once

bool GetDateFromString(string &eventName, string &s, string &day, int &date, int &month, int &year);

void SetYearToCurrentYear(int & year);

bool ValidateInput(string &userInput);

void GetEventNameFromString(std::string &userInput, std::string &eventName);

string DetermineUserInput(std::string &userInput);

void CreateRegx(std::vector<std::string> &testThing);

vector<string> Split(string &s, char del);

extern string file;

vector <string> FindInString(vector<string> rgx, string userInput);

string TrimString(string str1);

string DisplayDateTime(tm time);
